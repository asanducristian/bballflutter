import 'package:bball/MainPage.dart';
import 'package:bball/models/ResponseData.dart';
import 'package:bball/tools/HttpClient.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bball/transitions/FadeInRoute.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_hud/progress_hud.dart';

import 'EventsPage.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _loginForm = GlobalKey<FormState>();
  ProgressHUD _progressHUD;
  String password = "";
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    _progressHUD = new ProgressHUD(
      color: Colors.white,
      backgroundColor: Colors.transparent,
      containerColor: Colors.black,
      loading: false,
      text: 'Loading...',
    );
    super.initState();
  }

  void navigateToEventsPage() {
    Navigator.pushAndRemoveUntil(
        context, FadeInRoute(page: MainPage()), (r) => false);
  }

  Future<void> register() async {
    String udid = await FlutterUdid.udid;
    ResponseData response = await HttpClient.instance
        .register(usernameController.text.toLowerCase(), passwordController.text.toLowerCase(), udid);
    if (response.success == true) {
      navigateToEventsPage();
    } else {
      Fluttertoast.showToast(msg: response.message);
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('REGISTER',
            style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'High School Sans')),
      ),
      backgroundColor: Color(0xff3434ab),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Center(
              child: Form(
                key: _loginForm,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextFormField(
                        controller: usernameController,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelText: 'Username',
                            fillColor: Colors.white,
                            filled: true,
                            border: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            )),
                        style: TextStyle(fontFamily: 'High School Sans'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelText: 'Password',
                            fillColor: Colors.white,
                            filled: true,
                            border: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            )),
                        obscureText: true,
                        validator: (val) {
                          if (val.length < 8) {
                            return 'Password must be at elast 8 characters long';
                          } else {
                            this.password = val;
                            return null;
                          }
                        },
                        style: TextStyle(fontFamily: 'High School Sans'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextFormField(
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelText: 'Repeat password',
                            fillColor: Colors.white,
                            filled: true,
                            border: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            )),
                        obscureText: true,
                        validator: (val) {
                          if (val != password) {
                            return 'Passwords do not match';
                          } else {
                            return null;
                          }
                        },
                        style: TextStyle(fontFamily: 'High School Sans'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: GestureDetector(
                        onTap: () async {
                          if (_loginForm.currentState.validate()) {
                            _progressHUD.state.show();
                            await register();
                            _progressHUD.state.dismiss();
                          }
                        },
                        child: new Container(
                          child: new Text("Register",
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'High School Sans')),
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(10.0)),
                              color: Colors.white),
                          padding:
                              new EdgeInsets.fromLTRB(16.0, 13.0, 16.0, 13.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          _progressHUD,
        ],
      ),
    );
  }
}
