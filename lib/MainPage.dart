import 'package:bball/CreateEventPage.dart';
import 'package:bball/ProfilePage.dart';
import 'package:bball/EventsPage.dart';
import 'package:bball/transitions/FadeInRoute.dart';
import 'package:bball/transitions/SlideLeftRoute.dart';
import 'package:bball/transitions/SlideRightRoute.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'SettingsPage.dart';

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainPage();
}

class _MainPage extends State<MainPage> {
  int _currentIndex = 0;
  final List<String> _appBarTitles = ["Profile", "Events", "Settings"];
  final List<Icon> _icons = [Icon(Icons.settings), Icon(Icons.add)];
  final List<Widget> _children = [ProfilePage(), EventsPage(), ProfilePage()];
  final List<Widget> _rightButtonPages = [SettingsPage(), CreateEventPage()];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void rightButtonAction() {
    Navigator.push(context, SlideLeftRoute(page: _rightButtonPages[_currentIndex]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: _icons[_currentIndex],
            color: Colors.white,
            onPressed: () {
              rightButtonAction();
            },
          )
        ],
        title: Text(
          _appBarTitles[_currentIndex],
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: _children[_currentIndex], // new
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.white,
        selectedLabelStyle: TextStyle(
          color: Colors.white,
          fontSize: 13,
          fontFamily: 'High School Sans',
        ),
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.portrait),
            title: Text(
              'Profile',
              style: TextStyle(fontSize: 10, fontFamily: 'High School Sans'),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            title: Text(
              'Events',
              style: TextStyle(fontSize: 10, fontFamily: 'High School Sans'),
            ),
          ),
        ],
        backgroundColor: Color(0xff3434ab),
      ),
    );
  }
}
