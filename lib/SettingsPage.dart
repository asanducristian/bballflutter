import 'package:bball/AboutUsPage.dart';
import 'package:bball/EditProfilePage.dart';
import 'package:bball/LoginRegisterPage.dart';
import 'package:bball/tools/Constants.dart';
import 'package:bball/tools/Functions.dart';
import 'package:bball/transitions/FadeInRoute.dart';
import 'package:bball/transitions/SlideLeftRoute.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'ChangePasswordPage.dart';
import 'database/DatabaseClient.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Settings',
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      body: Container(
        color: Color(LIGHT_GREY),
        child: Column(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(top: 15.0, right: 15.0, left: 15.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30.0))),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 13.0, bottom: 13.0, right: 20.0, left: 20.0),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        // GestureDetector(
                        //   onTap: () {
                        //     Navigator.push(context,
                        //         SlideLeftRoute(page: ChangePasswordPage()));
                        //   },
                        //   child: Padding(
                        //     padding: const EdgeInsets.only(bottom: 8),
                        //     child: Row(
                        //       children: <Widget>[
                        //         Expanded(
                        //           child: Padding(
                        //             padding: const EdgeInsets.only(right: 10.0),
                        //             child: Icon(
                        //               Icons.lock,
                        //               color: Color(DARK_GREY),
                        //               size: 25.0,
                        //             ),
                        //           ),
                        //         ),
                        //         Expanded(
                        //           flex: 9,
                        //           child: Text(
                        //             "CHANGE PASSWORD",
                        //             style: TextStyle(
                        //                 color: Color(DARK_GREY),
                        //                 fontSize: 16,
                        //                 fontFamily: FONT_HIGH_SCHOOL),
                        //           ),
                        //         )
                        //       ],
                        //     ),
                        //   ),
                        // ),
                        // Divider(
                        //   height: 1.0,
                        //   color: Colors.grey,
                        // ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(context,
                                SlideLeftRoute(page: EditProfilePage()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(
                                      Icons.portrait,
                                      color: Color(DARK_GREY),
                                      size: 25.0,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 9,
                                  child: Text(
                                    "EDIT PROFILE",
                                    style: TextStyle(
                                        color: Color(DARK_GREY),
                                        fontSize: 16,
                                        fontFamily: FONT_HIGH_SCHOOL),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 15.0, right: 15.0, left: 15.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30.0))),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 13.0, bottom: 13.0, right: 20.0, left: 20.0),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.push(context,
                                SlideLeftRoute(page: AboutUsPage()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(
                                      Icons.people,
                                      color: Color(DARK_GREY),
                                      size: 25.0,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 9,
                                  child: Text(
                                    "ABOUT US",
                                    style: TextStyle(
                                        color: Color(DARK_GREY),
                                        fontSize: 16,
                                        fontFamily: FONT_HIGH_SCHOOL),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Divider(
                          height: 1.0,
                          color: Colors.grey,
                        ),
                        GestureDetector(
                          onTap: () {
                            showCustomDialog(
                                context,
                                "Are you sure you want to log out?",
                                "LOGOUT",
                                "CANCEL", () async {
                              (await (await DatabaseClient.instance)
                                  .setAppSettingsLogon(false, 0));
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  FadeInRoute(page: LoginRegisterPage()),
                                  (r) => false);
                            }, () {});
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(
                                      Icons.exit_to_app,
                                      color: Color(DARK_GREY),
                                      size: 25.0,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 9,
                                  child: Text(
                                    "LOGOUT",
                                    style: TextStyle(
                                        color: Color(DARK_GREY),
                                        fontSize: 16,
                                        fontFamily: FONT_HIGH_SCHOOL),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
