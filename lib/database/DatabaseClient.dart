import 'dart:developer';

import 'package:bball/database/MyDatabase.dart';
import 'package:bball/models/AppSettings.dart';
import 'package:bball/models/User.dart';
import 'package:floor/floor.dart';

class DatabaseClient {
  DatabaseClient._privateConstructor();

  static DatabaseClient _instance = DatabaseClient._privateConstructor();

  MyDatabase database;

  static Future<DatabaseClient> get instance async {
    if (_instance.database == null) {
      log("database created");
      _instance.database = await $FloorMyDatabase
          .databaseBuilder('my_database.db')
          .addMigrations([
        migration1to2(),
        migration2to3(),
        migration3to4(),
        migration4to5(),
        migration5to6(),
      ]).build();
    }
    return _instance;
  }

  static Future<DatabaseClient> get newInstance async {
    _instance = DatabaseClient._privateConstructor();
    _instance.database =
        await $FloorMyDatabase.databaseBuilder('my_database.db').addMigrations([
      migration1to2(),
      migration2to3(),
      migration3to4(),
      migration4to5(),
      migration5to6(),
    ]).build();
    return _instance;
  }

  Future<void> saveUser(User user) async {
    return await _instance.database.userDao.insertUser(user);
  }

  Future<void> deleteUser() async {
    return await _instance.database.userDao.deleteUser();
  }

  Future<User> getUser() async {
    User user = await _instance.database.userDao.getUser();
    return user;
  }

  Future<void> saveAppSettings(AppSettings appSettings) async {
    return await _instance.database.appSettingsDAO
        .insertAppSettings(appSettings);
  }

  Future<void> deleteAppSettings() async {
    return await _instance.database.appSettingsDAO.deleteAppSettings();
  }

  Future<AppSettings> getAppSettings() async {
    AppSettings appSettings =
        await _instance.database.appSettingsDAO.getAppSettings();
    return appSettings;
  }

  Future<void> setAppSettingsLogon(bool loggedOn, int userId) async {
    AppSettings appSettings =
        (await _instance.database.appSettingsDAO.getAppSettings());
    appSettings.loggedOn = true;
    appSettings.userId = userId;
    AppSettings appSettingsToSave = AppSettings(
        0, appSettings.onBoardingPresented, true, appSettings.userId, appSettings.onBoardDone);
    var result = await _instance.database.appSettingsDAO.deleteAppSettings();
    result = await _instance.database.appSettingsDAO
        .insertAppSettings(appSettingsToSave);
  }

  static Migration migration1to2() {
// create migration
    return Migration(1, 2, (database) {
      database.execute(
          'ALTER TABLE user ADD COLUMN `noOfEventsEnrolled` INTEGER;ALTER TABLE user ADD COLUMN `noOfEventsHonoured` INTEGER');
    });
  }

  static Migration migration2to3() {
// create migration
    return Migration(2, 3, (database) {
      database.execute(
          'ALTER TABLE user ADD COLUMN `noOfEventsEnrolled` INTEGER;ALTER TABLE user ADD COLUMN `noOfEventsHonoured` INTEGER');
    });
  }

  static Migration migration3to4() {
// create migration
    return Migration(3, 4, (database) {
      database.execute(
          'CREATE TABLE IF NOT EXISTS `AppSettings` (`id` INTEGER, `onBoardingPresented` INTEGER, `loggedOn` INTEGER, `userId` INTEGER, PRIMARY KEY (`id`))');
    });
  }

  static Migration migration4to5() {
// create migration
    return Migration(4, 5, (database) {
      database.execute('');
    });
  }

  static Migration migration5to6() {
// create migration
    return Migration(5, 6, (database) {
      database
          .execute('ALTER TABLE AppSettings ADD COLUMN `onBoardDone` INTEGER');
    });
  }
}
