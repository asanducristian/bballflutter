import 'package:bball/models/AppSettings.dart';
import 'package:bball/models/User.dart';
import 'package:floor/floor.dart';
import '../models/User.dart';
import '../database/daos/UserDAO.dart';


 // required package imports
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'daos/AppSettingsDAO.dart';    

part 'MyDatabase.g.dart';

@Database(version: 6, entities: [User, AppSettings])
abstract class MyDatabase extends FloorDatabase {
  UserDAO get userDao;
  AppSettingsDAO get appSettingsDAO;
}
