import 'package:bball/models/AppSettings.dart';
import 'package:floor/floor.dart';

@dao
abstract class AppSettingsDAO {
  @Query('SELECT * FROM AppSettings LIMIT 1')
  Future<AppSettings> getAppSettings();

  @insert
  Future<void> insertAppSettings(AppSettings appSettings);

  @Query('DELETE FROM AppSettings')
  Future<void> deleteAppSettings();

  @Query('UPDATE AppSettings SET loggedOn = :loggedOn where id = 0')
  Future<void> setLoggedOn(bool loggedOn);
}
