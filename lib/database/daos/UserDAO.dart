import 'package:bball/models/User.dart';
import 'package:floor/floor.dart';

@dao
abstract class UserDAO {
  @Query('SELECT * FROM User LIMIT 1')
  Future<User> getUser();

  @insert
  Future<void> insertUser(User user);

  @Query('DELETE FROM User')
  Future<void> deleteUser();
}
