// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MyDatabase.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorMyDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$MyDatabaseBuilder databaseBuilder(String name) =>
      _$MyDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$MyDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$MyDatabaseBuilder(null);
}

class _$MyDatabaseBuilder {
  _$MyDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$MyDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$MyDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<MyDatabase> build() async {
    final database = _$MyDatabase();
    database.database = await database.open(
      name ?? ':memory:',
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$MyDatabase extends MyDatabase {
  _$MyDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  UserDAO _userDaoInstance;

  AppSettingsDAO _appSettingsDAOInstance;

  Future<sqflite.Database> open(String name, List<Migration> migrations,
      [Callback callback]) async {
    final path = join(await sqflite.getDatabasesPath(), name);

    return sqflite.openDatabase(
      path,
      version: 6,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `User` (`id` INTEGER, `cityId` INTEGER, `username` TEXT, `udid` TEXT, `alias` TEXT, `userType` TEXT, `trustPoints` INTEGER, `prefeerdSportId` INTEGER, `lastName` TEXT, `firstName` TEXT, `noOfEventsEnrolled` INTEGER, `noOfEventsHonoured` INTEGER, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `AppSettings` (`id` INTEGER, `onBoardingPresented` INTEGER, `loggedOn` INTEGER, `userId` INTEGER, `onBoardDone` INTEGER, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
  }

  @override
  UserDAO get userDao {
    return _userDaoInstance ??= _$UserDAO(database, changeListener);
  }

  @override
  AppSettingsDAO get appSettingsDAO {
    return _appSettingsDAOInstance ??=
        _$AppSettingsDAO(database, changeListener);
  }
}

class _$UserDAO extends UserDAO {
  _$UserDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _userInsertionAdapter = InsertionAdapter(
            database,
            'User',
            (User item) => <String, dynamic>{
                  'id': item.id,
                  'cityId': item.cityId,
                  'username': item.username,
                  'udid': item.udid,
                  'alias': item.alias,
                  'userType': item.userType,
                  'trustPoints': item.trustPoints,
                  'prefeerdSportId': item.prefeerdSportId,
                  'lastName': item.lastName,
                  'firstName': item.firstName,
                  'noOfEventsEnrolled': item.noOfEventsEnrolled,
                  'noOfEventsHonoured': item.noOfEventsHonoured
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _userMapper = (Map<String, dynamic> row) => User(
      row['id'] as int,
      row['cityId'] as int,
      row['username'] as String,
      row['udid'] as String,
      row['alias'] as String,
      row['userType'] as String,
      row['trustPoints'] as int,
      row['prefeerdSportId'] as int,
      row['lastName'] as String,
      row['firstName'] as String,
      row['noOfEventsEnrolled'] as int,
      row['noOfEventsHonoured'] as int);

  final InsertionAdapter<User> _userInsertionAdapter;

  @override
  Future<User> getUser() async {
    return _queryAdapter.query('SELECT * FROM User LIMIT 1',
        mapper: _userMapper);
  }

  @override
  Future<void> deleteUser() async {
    await _queryAdapter.queryNoReturn('DELETE FROM User');
  }

  @override
  Future<void> insertUser(User user) async {
    await _userInsertionAdapter.insert(user, sqflite.ConflictAlgorithm.abort);
  }
}

class _$AppSettingsDAO extends AppSettingsDAO {
  _$AppSettingsDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _appSettingsInsertionAdapter = InsertionAdapter(
            database,
            'AppSettings',
            (AppSettings item) => <String, dynamic>{
                  'id': item.id,
                  'onBoardingPresented': item.onBoardingPresented ? 1 : 0,
                  'loggedOn': item.loggedOn ? 1 : 0,
                  'userId': item.userId,
                  'onBoardDone': item.onBoardDone
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _appSettingsMapper = (Map<String, dynamic> row) => AppSettings(
      row['id'] as int,
      (row['onBoardingPresented'] as int) != 0,
      (row['loggedOn'] as int) != 0,
      row['userId'] as int,
      row['onBoardDone'] as int);

  final InsertionAdapter<AppSettings> _appSettingsInsertionAdapter;

  @override
  Future<AppSettings> getAppSettings() async {
    return _queryAdapter.query('SELECT * FROM AppSettings LIMIT 1',
        mapper: _appSettingsMapper);
  }

  @override
  Future<void> deleteAppSettings() async {
    await _queryAdapter.queryNoReturn('DELETE FROM AppSettings');
  }

  @override
  Future<void> setLoggedOn(bool loggedOn) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE AppSettings SET loggedOn = ? where id = 0',
        arguments: <dynamic>[loggedOn]);
  }

  @override
  Future<void> insertAppSettings(AppSettings appSettings) async {
    await _appSettingsInsertionAdapter.insert(
        appSettings, sqflite.ConflictAlgorithm.abort);
  }
}
