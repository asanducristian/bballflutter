import 'package:bball/tools/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AboutUsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AboutUsPage();
}

class _AboutUsPage extends State<AboutUsPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'About us',
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      backgroundColor: Color(LIGHT_GREY),
    );
  }
}
