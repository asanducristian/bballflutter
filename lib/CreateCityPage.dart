import 'package:bball/tools/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:progress_hud/progress_hud.dart';


class CreateCityPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreateCityPage();
}

class _CreateCityPage extends State<CreateCityPage> {
  ProgressHUD _progressHUD;

  final cityNameController = TextEditingController();
  final countryNameController = TextEditingController();

  final _cityForm = GlobalKey<FormState>();
  
  @override
  void initState() {
    _progressHUD = new ProgressHUD(
      color: Colors.white,
      backgroundColor: Colors.transparent,
      containerColor: Colors.black,
      loading: false,
      text: 'Loading...',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Create City",
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        color: Color(LIGHT_GREY),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 20.0),
              child: Center(
                child: Form(
                  key: _cityForm,
                  child: ListView(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: TextFormField(
                              validator: (val){
                                if(val==""){
                                  return "This field can not be empty!";
                                } else {
                                  return null;
                                }
                              },
                              controller: cityNameController,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'City Name',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: TextFormField(
                              validator: (val){
                                if(val==""){
                                  return "This field can not be empty!";
                                } else {
                                  return null;
                                }
                              },
                              controller: countryNameController,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'Country',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(24.0),
                            child: GestureDetector(
                              onTap: () {
                                if(_cityForm.currentState.validate()){
                                  print(cityNameController.text+"    "+countryNameController.text);
                                }
                              },
                              child: new Container(
                                child: new Text("create city",
                                    style: new TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'High School Sans')),
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.all(
                                        new Radius.circular(10.0)),
                                    color: Colors.white),
                                padding: new EdgeInsets.fromLTRB(
                                    16.0, 13.0, 16.0, 13.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _progressHUD,
          ],
        ),
      ),
    );
  }
}
