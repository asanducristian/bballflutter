import 'package:bball/models/BaseClass.dart';

class Sport extends BaseClass {
  String name;
  int sport_id;
  bool approved;

  Sport(this.name, this.sport_id, this.approved);

  static Sport fromJson(cityMap) {
    Sport city = Sport(cityMap['name'], cityMap['sport_id'],
        cityMap['approved']);
    return city;
  }

  static List<Sport> fromJsonArray(jsonArray) {
    List<Sport> cities = [];
    for (var jsonObject in jsonArray) {
      cities.add(Sport.fromJson(jsonObject));
    }
    return cities;
  }
}
