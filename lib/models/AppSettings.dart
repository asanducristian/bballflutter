
import 'package:floor/floor.dart';

@entity
class AppSettings{
  @primaryKey
  int id = 0;
  bool onBoardingPresented=false;
  bool loggedOn=false;
  int userId=0;
  int onBoardDone=0;

  AppSettings(
    this.id,
    this.onBoardingPresented,
    this.loggedOn,
    this.userId,
    this.onBoardDone,
  );
}