import 'package:bball/tools/Constants.dart';

import 'BaseClass.dart';
import 'City.dart';
import 'User.dart';

class ResponseData {
  final bool success;
  final String message;
  final BaseClass data;
  final List<BaseClass> multipleData;

  ResponseData(this.data, this.success, this.message, this.multipleData);

  static ResponseData fromJson(responseMap, dataType) {
    BaseClass data = BaseClass();
    List<BaseClass> multipleData = [];
    if (responseMap['success']) {
      switch (dataType) {
        case USER_DATA_TYPE:
          {
            data = User.fromJson(responseMap['data']['user']);
            break;
          }
        case CITY_DATA_TYPE:
          {
            multipleData = City.fromJsonArray(responseMap['data']['cities']);
            break;
          }
        case SPORT_DATA_TYPE:
          {
            multipleData = City.fromJsonArray(responseMap['data']['sports']);
            break;
          }
      }
    }
    ResponseData responseData = ResponseData(
        data, responseMap['success'], responseMap['message'], multipleData);
    return responseData;
  }
}
