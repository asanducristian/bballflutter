import 'package:bball/models/BaseClass.dart';
import 'package:floor/floor.dart';

@entity
class User extends BaseClass {
  @primaryKey
  int id = 0;
  int cityId = 0;
  String username = "";
  String udid = "";
  String alias = "";
  String userType = "";
  int trustPoints = 0;
  int prefeerdSportId = 0;
  String lastName = "";
  String firstName = "";
  int noOfEventsEnrolled = 0;
  int noOfEventsHonoured = 0;

  User(
      this.id,
      this.cityId,
      this.username,
      this.udid,
      this.alias,
      this.userType,
      this.trustPoints,
      this.prefeerdSportId,
      this.lastName,
      this.firstName,
      this.noOfEventsEnrolled,
      this.noOfEventsHonoured);

  User.construct();

  static User fromJson(userMap) {
    User user = User(
        userMap['user_id'],
        userMap['city_id'],
        userMap['username'],
        userMap['udid'],
        userMap['alias'],
        userMap['user_type'],
        userMap['trust_points'],
        userMap['prefered_sport_id'],
        userMap['last_name'],
        userMap['first_name'],
        userMap['attendances']['noOfEventsEnrolled'],
        userMap['attendances']['noOfEventsHonoured']);
    return user;
  }
}
