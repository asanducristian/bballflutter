import 'package:bball/models/BaseClass.dart';

class City extends BaseClass {
  String name;
  String country;
  int city_id;
  bool approved;

  City(this.name, this.country, this.city_id, this.approved);

  static City fromJson(cityMap) {
    City city = City(cityMap['name'], cityMap['country'], cityMap['city_id'],
        cityMap['approved']);
    return city;
  }

  static List<City> fromJsonArray(jsonArray) {
    List<City> cities = [];
    for (var jsonObject in jsonArray) {
      cities.add(City.fromJson(jsonObject));
    }
    return cities;
  }
}
