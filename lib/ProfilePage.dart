import 'package:bball/database/DatabaseClient.dart';
import 'package:bball/tools/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'models/User.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {
  double _progressValue = 0;
  String _nameToShow = "";
  String _alias = "";
  String _trustScore = "";
  User user = User.construct();

  void getUser() async {
    user = await (await DatabaseClient.instance).getUser();
    this.setState(() {
      var fullNameToShow="John Doe";
      if(user.firstName != null){
        fullNameToShow="";
        fullNameToShow+=user.firstName;
      }
      if(user.lastName !=null){
        fullNameToShow+=" "+user.lastName;
      }
      _nameToShow = fullNameToShow;
      _alias = user.alias == null ? "" : "- ${user.alias} -";
      _trustScore = getTrustScore(user);
      _progressValue = getProgressValue(user);
    });
  }

  String getTrustScore(User user) {
    if (user.noOfEventsEnrolled == 0) return "100%";

    if (user.noOfEventsHonoured == 0) return "0%";

    return "${((user.noOfEventsHonoured / user.noOfEventsEnrolled) * 100).round()}%";
  }

  double getProgressValue(User user) {
    if (user.noOfEventsEnrolled == 0) return 1;

    if (user.noOfEventsHonoured == 0) return 0;

    return user.noOfEventsHonoured / user.noOfEventsEnrolled;
  }

  @override
  void initState() {
    super.initState();
    this.getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: SizedBox.expand(
              child: Container(
                color: Color(0xff3434ab),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: new BorderRadius.circular(200),
                      child: Image.network(
                        'https://picsum.photos/200/300',
                        fit: BoxFit.cover,
                        height: 100.0,
                        width: 100.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        _nameToShow,
                        style: TextStyle(
                          backgroundColor: Color(0xff3434ab),
                          color: Colors.white,
                          fontSize: 15,
                          fontFamily: FONT_HIGH_SCHOOL,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Text(
                        _alias,
                        style: TextStyle(
                          backgroundColor: Color(0xff3434ab),
                          color: Colors.white,
                          fontSize: 11,
                          fontFamily: FONT_HIGH_SCHOOL,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 7,
            child: SizedBox.expand(
              child: Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Container(
                                  child: Center(
                                child: Text(
                                  _trustScore,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontFamily: FONT_HIGH_SCHOOL),
                                ),
                              ))),
                          Expanded(
                              flex: 8,
                              child: Container(
                                width: 100,
                                height: 18,
                                child: LinearProgressIndicator(
                                  value: _progressValue,
                                  backgroundColor: Colors.grey,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color(BASE_COLOR)),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
