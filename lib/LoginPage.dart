import 'dart:convert';
import 'dart:io';
import 'package:bball/models/ResponseData.dart';
import 'package:bball/tools/HttpClient.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bball/transitions/FadeInRoute.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dio/dio.dart';
import 'package:progress_hud/progress_hud.dart';

import 'EventsPage.dart';
import 'MainPage.dart';
import 'models/User.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  ProgressHUD _progressHUD;

  final _loginForm = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    _progressHUD = new ProgressHUD(
      color: Colors.white,
      backgroundColor: Colors.transparent,
      containerColor: Colors.black,
      loading: false,
      text: 'Loading...',
    );
    super.initState();
  }

  void navigateTooMainPage() {
    Navigator.pushAndRemoveUntil(
        context, FadeInRoute(page: MainPage()), (r) => false);
  }

  Future<void> login() async {
    ResponseData responseData = await HttpClient.instance
        .login(usernameController.text.toLowerCase(), passwordController.text.toLowerCase());

    if (responseData.success) {
      navigateTooMainPage();
    } else {
      Fluttertoast.showToast(msg: responseData.message);
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'LOGIN',
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      backgroundColor: Color(0xff3434ab),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Center(
              child: Form(
                key: _loginForm,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextFormField(
                        controller: usernameController,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelText: 'Username',
                            fillColor: Colors.white,
                            filled: true,
                            border: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            )),
                        style: TextStyle(fontFamily: 'High School Sans'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelText: 'Password',
                            fillColor: Colors.white,
                            filled: true,
                            border: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            )),
                        obscureText: true,
                        validator: (val) {
                          if (val.length < 8) {
                            return 'Password must be at elast 8 characters long';
                          } else {
                            return null;
                          }
                        },
                        style: TextStyle(fontFamily: 'High School Sans'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: GestureDetector(
                        onTap: () async {
                          _progressHUD.state.show();
                          await login();
                          _progressHUD.state.dismiss();
                        },
                        child: new Container(
                          child: new Text("Login",
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'High School Sans')),
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(10.0)),
                              color: Colors.white),
                          padding:
                              new EdgeInsets.fromLTRB(16.0, 13.0, 16.0, 13.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          _progressHUD,
        ],
      ),
    );
  }
}
