import 'package:bball/tools/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CreateEventPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreateEventPage();
}

class _CreateEventPage extends State<CreateEventPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Create event',
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      backgroundColor: Color(LIGHT_GREY),
    );
  }
}
