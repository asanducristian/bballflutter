import 'package:bball/CreateEventPage.dart';
import 'package:bball/ProfilePage.dart';
import 'package:bball/EventsPage.dart';
import 'package:bball/tools/Constants.dart';
import 'package:bball/transitions/FadeInRoute.dart';
import 'package:bball/transitions/SlideLeftRoute.dart';
import 'package:bball/transitions/SlideRightRoute.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:progress_hud/progress_hud.dart';

import 'SettingsPage.dart';

class CreateSportPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreateSportPage();
}

class _CreateSportPage extends State<CreateSportPage> {
  ProgressHUD _progressHUD;
  
  @override
  void initState() {
    _progressHUD = new ProgressHUD(
      color: Colors.white,
      backgroundColor: Colors.transparent,
      containerColor: Colors.black,
      loading: false,
      text: 'Loading...',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Create City",
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        color: Color(LIGHT_GREY),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 20.0),
              child: Center(
                child: Form(
                  child: ListView(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: TextFormField(
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'First Name',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: TextFormField(
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'Last Name',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(24.0),
                            child: GestureDetector(
                              onTap: () {
                                print("SAVE");
                              },
                              child: new Container(
                                child: new Text("create city",
                                    style: new TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'High School Sans')),
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.all(
                                        new Radius.circular(10.0)),
                                    color: Colors.white),
                                padding: new EdgeInsets.fromLTRB(
                                    16.0, 13.0, 16.0, 13.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _progressHUD,
          ],
        ),
      ),
    );
  }
}
