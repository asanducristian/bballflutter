import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:bball/CreateCityPage.dart';
import 'package:bball/CreateSportPage.dart';
import 'package:bball/tools/Constants.dart';
import 'package:bball/tools/HttpClient.dart';
import 'package:bball/transitions/SlideLeftRoute.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_hud/progress_hud.dart';

import 'models/City.dart';
import 'models/ResponseData.dart';

class EditProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _EditProfilePage();
}

class _EditProfilePage extends State<EditProfilePage> {
  ProgressHUD _progressHUD;

  final _editUserForm = GlobalKey<FormState>();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final aliasController = TextEditingController();

  GlobalKey<AutoCompleteTextFieldState<String>> citiesKey = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> sportsKey = new GlobalKey();

  List<City> cities;
  List<String> cityStrings = [];

  List<City> sports;
  List<String> sportStrings = [];

  @override
  void initState() {
    _progressHUD = new ProgressHUD(
      color: Colors.white,
      backgroundColor: Colors.transparent,
      containerColor: Colors.black,
      loading: false,
      text: 'Loading...',
    );
    getCities();
    getSports();
    super.initState();
  }

  Future<void> getCities() async {
    ResponseData response = await HttpClient.instance.getCities();
    if (response.success) {
      cities = response.multipleData;
      for (var city in cities) {
        cityStrings.add(city.name);
      }
    } else {
      Fluttertoast.showToast(msg: "error");
    }
    return;
  }

  Future<void> getSports() async {
    ResponseData response = await HttpClient.instance.getSports();
    if (response.success) {
      sports = response.multipleData;
      for (var sport in sports) {
        sportStrings.add(sport.name);
      }
    } else {
      Fluttertoast.showToast(msg: "error");
    }
    return;
  }
  
  void navigateToCreateCityPage() {
    Navigator.push(context, SlideLeftRoute(page: (CreateCityPage())));
  }

  void navigateToCreateSportPage() {
    Navigator.push(context, SlideLeftRoute(page: (CreateSportPage())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'EDIT PROFILE',
          style: TextStyle(
            backgroundColor: Color(0xff3434ab),
            color: Colors.white,
            fontSize: 15,
            fontFamily: 'High School Sans',
          ),
        ),
      ),
      body: Container(
        color: Color(LIGHT_GREY),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 20.0, left: 20.0),
              child: Center(
                child: Form(
                  key: _editUserForm,
                  child: ListView(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: TextFormField(
                              controller: firstNameController,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'First Name',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: TextFormField(
                              controller: lastNameController,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'Last Name',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: TextFormField(
                              controller: aliasController,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  labelText: 'Alias',
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: UnderlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  )),
                              style: TextStyle(fontFamily: 'High School Sans'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 10,
                                  child: AutoCompleteTextField<String>(
                                      key: citiesKey,
                                      style: new TextStyle(
                                          fontFamily: 'High School Sans',
                                          color: Colors.black,
                                          fontSize: 16.0),
                                      decoration: InputDecoration(
                                          labelStyle: TextStyle(
                                              fontFamily: 'High School Sans'),
                                          contentPadding:
                                              EdgeInsets.fromLTRB(10, 5, 10, 5),
                                          labelText: 'Main City',
                                          fillColor: Colors.white,
                                          filled: true,
                                          border: UnderlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          )),
                                      suggestions: cityStrings,
                                      itemBuilder: (context, item) {
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            item,
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12,
                                              fontFamily: 'High School Sans',
                                            ),
                                          ),
                                        );
                                      },
                                      itemSubmitted: (item){
                                        print(item);
                                        throw(new Exception());
                                      },
                                      itemFilter: (item, query) {
                                        return item
                                            .toLowerCase()
                                            .startsWith(query.toLowerCase());
                                      }),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: IconButton(
                                    icon: Icon(Icons.add),
                                    color: Colors.white,
                                    iconSize: 30,
                                    onPressed: () {
                                      navigateToCreateCityPage();
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 3),
                            child: Text(
                              "If you cannot find your city please feel free to create it by pressing the plus icon, it will be approved by a moderator in maximum 48 hours.",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                                fontFamily: 'High School Sans',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 10,
                                  child: AutoCompleteTextField<String>(
                                      key: sportsKey,
                                      style: new TextStyle(
                                          fontFamily: 'High School Sans',
                                          color: Colors.black,
                                          fontSize: 16.0),
                                      decoration: InputDecoration(
                                          labelStyle: TextStyle(
                                              fontFamily: 'High School Sans'),
                                          contentPadding:
                                              EdgeInsets.fromLTRB(10, 5, 10, 5),
                                          labelText: 'Main Sport',
                                          fillColor: Colors.white,
                                          filled: true,
                                          border: UnderlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          )),
                                      suggestions: sportStrings,
                                      itemBuilder: (context, item) {
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            item,
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12,
                                              fontFamily: 'High School Sans',
                                            ),
                                          ),
                                        );
                                      },
                                      itemSubmitted: (item){
                                        print(item);
                                        throw(new Exception());
                                      },
                                      itemFilter: (item, query) {
                                        return item
                                            .toLowerCase()
                                            .startsWith(query.toLowerCase());
                                      }),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: IconButton(
                                    icon: Icon(Icons.add),
                                    color: Colors.white,
                                    iconSize: 30,
                                    onPressed: () {
                                      navigateToCreateSportPage();
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 3),
                            child: Text(
                              "If you cannot find your sport please feel free to create it by pressing the plus icon, it will be approved by a moderator in maximum 48 hours.",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                                fontFamily: 'High School Sans',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(24.0),
                            child: GestureDetector(
                              onTap: () {
                                print("SAVE");
                              },
                              child: new Container(
                                child: new Text("save changes",
                                    style: new TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'High School Sans')),
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.all(
                                        new Radius.circular(10.0)),
                                    color: Colors.white),
                                padding: new EdgeInsets.fromLTRB(
                                    16.0, 13.0, 16.0, 13.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _progressHUD,
          ],
        ),
      ),
    );
  }
}
