import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bball/transitions/FadeInRoute.dart';

import 'LoginPage.dart';
import 'RegisterPage.dart';

class LoginRegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginRegisterPageState();
}

class _LoginRegisterPageState extends State<LoginRegisterPage> {
  void navigateToRegister() {
    Navigator.push(context, FadeInRoute(page: RegisterPage()));
  }

  void navigateToLogin() {
    Navigator.push(context, FadeInRoute(page: LoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff3434ab),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      color: Colors.white,
                      height: 70,
                      width: 70,
                      image: AssetImage('assets/images/logo.png'),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        'BBall',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontFamily: 'High School Sans',
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        navigateToLogin();
                      },
                      child: new Container(
                        child: new Text("I already have an account",
                            style: new TextStyle(
                                color: Colors.black,
                                fontFamily: 'High School Sans')),
                        decoration: new BoxDecoration(
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(10.0)),
                            color: Colors.white),
                        padding:
                            new EdgeInsets.fromLTRB(16.0, 13.0, 16.0, 13.0),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        navigateToRegister();
                      },
                      child: new Container(
                        child: new Text("Create a new account",
                            style: new TextStyle(
                                color: Colors.black,
                                fontFamily: 'High School Sans')),
                        decoration: new BoxDecoration(
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(10.0)),
                            color: Colors.white),
                        padding:
                            new EdgeInsets.fromLTRB(16.0, 13.0, 16.0, 13.0),
                      ),
                    ),
                  ),
                ],
              )),
            )
          ],
        ),
      ),
    );
  }
}
