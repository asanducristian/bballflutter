import 'dart:convert';

import 'package:bball/database/DatabaseClient.dart';
import 'package:bball/models/AppSettings.dart';
import 'package:bball/models/City.dart';
import 'package:bball/models/ResponseData.dart';
import 'package:bball/models/User.dart';

import 'Constants.dart' as Constants;
import 'package:dio/dio.dart';

class HttpClient {
  HttpClient._privateConstructor();

  static HttpClient _instance = HttpClient._privateConstructor();

  Dio http;

  static HttpClient get instance {
    if (_instance.http == null) {
      _instance.http = Dio(BaseOptions(
          baseUrl: Constants.BASE_URL,
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          }));
    }
    return _instance;
  }

  static HttpClient get newInstance {
    _instance = HttpClient._privateConstructor();
    _instance.http = Dio(BaseOptions(
        baseUrl: Constants.BASE_URL,
        followRedirects: false,
        validateStatus: (status) {
          return status < 500;
        }));
    return _instance;
  }

  Future<ResponseData> login(String username, String password) async {
    Map<String, String> loginData = {
      'username': username,
      'password': password,
    };
    var data = json.encode(loginData);
    var responseData = ResponseData.fromJson(
        json.decode((await http.post("/login", data: data)).toString()),
        Constants.USER_DATA_TYPE);

    (await (await DatabaseClient.instance)
        .setAppSettingsLogon(true, (responseData.data as User).id));
    var result = (await (await DatabaseClient.instance).deleteUser());
    result = (await (await DatabaseClient.instance)
        .saveUser(responseData.data as User));
    return responseData;
  }

  Future<ResponseData> getUser(String userId) async {
    var responseData = ResponseData.fromJson(
        json.decode((await http.get("/get-user/${userId}")).toString()),
        Constants.USER_DATA_TYPE);
    var result = (await (await DatabaseClient.instance).deleteUser());
    result = (await (await DatabaseClient.instance)
        .saveUser(responseData.data as User));
    return responseData;
  }

  Future<ResponseData> getCities() async {
    var responseData = ResponseData.fromJson(
        json.decode((await http.get("/get-cities")).toString()),
        Constants.CITY_DATA_TYPE);
    return responseData;
  }

  Future<ResponseData> getSports() async {
    var responseData = ResponseData.fromJson(
        json.decode((await http.get("/get-sports")).toString()),
        Constants.SPORT_DATA_TYPE);
    return responseData;
  }

  Future<ResponseData> register(
      String username, String password, String udid) async {
    Map<String, String> registerData = {
      'username': username,
      'password': password,
      'udid': udid
    };
    var responseData = ResponseData.fromJson(
        json.decode(
            (await http.post("/register", data: registerData)).toString()),
        Constants.USER_DATA_TYPE);
    if (responseData.success) {
      var result = (await (await DatabaseClient.instance).deleteUser());
      result = (await (await DatabaseClient.instance)
          .saveUser(responseData.data as User));
    }
    return responseData;
  }
}
