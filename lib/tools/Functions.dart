import 'package:flutter/material.dart';

import 'Constants.dart';

void showCustomDialog(
    BuildContext context,
    String title,
    String acceptText,
    String cancelText,
    Function acceptFunction,
    Function cancelFunction) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: new Text(
          title,
          style: TextStyle(
              color: Colors.black, fontFamily: FONT_HIGH_SCHOOL, fontSize: 17),
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text(
              acceptText,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontFamily: FONT_HIGH_SCHOOL),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              acceptFunction();
            },
          ),
          new FlatButton(
            child: new Text(
              cancelText,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontFamily: FONT_HIGH_SCHOOL),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              cancelFunction();
            },
          ),
        ],
      );
    },
  );
}
