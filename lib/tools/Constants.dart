library Constants;

const String BASE_URL = "http://165.22.25.144/api";
const BASE_COLOR = 0xff3434ab;
const String FONT_HIGH_SCHOOL = 'High School Sans';

//DATA TYPES
const USER_DATA_TYPE = 1;
const CITY_DATA_TYPE = 2;
const SPORT_DATA_TYPE = 3;

//COLORS
const LIGHT_GREY = 0xffc7c7c7;
const DARK_GREY = 0xff4f4f4f;
