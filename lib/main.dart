import 'dart:async';

import 'package:bball/LoginRegisterPage.dart';
import 'package:bball/MainPage.dart';
import 'package:bball/database/daos/AppSettingsDAO.dart';
import 'package:bball/tools/HttpClient.dart';
import 'package:bball/transitions/FadeInRoute.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'database/DatabaseClient.dart';
import 'models/AppSettings.dart';

void main() => runApp(BballApp());

class BballApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BBall',
      debugShowCheckedModeBanner: false,
      theme:
          ThemeData(primaryColor: Color(0xff3434ab), accentColor: Colors.black),
      home: SplashScreen(title: 'Flutter Demo Home Page'),
      routes: <String, WidgetBuilder>{
        '/LoginRegisterPage': (BuildContext context) => new LoginRegisterPage()
      },
    );
  }
}

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SplashScreenState createState() {
    _SplashScreenState state = new _SplashScreenState();
    return state;
  }
}

class _SplashScreenState extends State<SplashScreen> {
  bool _called = false;
  void navigateToLoginRegister() {
    Navigator.push(context, FadeInRoute(page: LoginRegisterPage()));
  }

  void navigateToMainPage() {
    Navigator.pushAndRemoveUntil(
        context, FadeInRoute(page: MainPage()), (r) => false);
  }

  void checkLogin() async {
    var appSettings = (await (await DatabaseClient.instance).getAppSettings());
    if (appSettings == null) {
      (await (await DatabaseClient.instance).saveAppSettings(AppSettings(0,false, false, 0, 0)));
      navigateToLoginRegister();
    } else {
      if (appSettings.userId != 0) {
        (await (await HttpClient.instance)
            .getUser(appSettings.userId.toString()).then((onValue){

            }).catchError((onError){
              navigateToLoginRegister();
            }));
        navigateToMainPage();
      } else {
        navigateToLoginRegister();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!_called) {
      _called = true;
      checkLogin();
    }
    return Scaffold(
      backgroundColor: Color(0xff3434ab),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {checkLogin();},
              child: Image(
                color: Colors.white,
                height: 70,
                width: 70,
                image: AssetImage('assets/images/logo.png'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                'BBall',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                  fontFamily: 'High School Sans',
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
